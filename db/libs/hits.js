const {v4} = require('uuid')
const { Op } = require('sequelize')
const moment = require('moment')
module.exports = function setupHits (HitsModel, TagsModel) {

  function createListHits (list) {
    return new Promise(async (resolve, reject) => {
      try {
        let count = 0, detail = {}
        for (let item of list){
          item.idHits = v4()
          item.container = JSON.stringify(item)
          item.currentDate = moment().toDate()
          item.status = 'A'
          await HitsModel.create(item).then(async (result) => {
            count = count + 1
            const {idHits} = result
            for (let tag of item._tags){
              detail.idTags = v4()
              detail.idHits = idHits
              detail.description = tag
              await TagsModel.create(detail).catch(e=>{
                reject(e)
              })
            }
          }).catch(e=>{
            reject(e)
          })
        }
        resolve(count)
      }catch (e) {
        reject(e)
      }
    })
  }

  function findData({ where, page, size }){
    return new Promise(async (resolve, reject)=>{
      const offset = size * page
      const limit = offset + size
      let whereQuery = { status: 'A' }
      let whereQueryDetail = { }
      if (where && where.author) {
        whereQuery['author'] = { [Op.like]: `%${where.author}%` }
      }
      if (where && where.title) {
        whereQuery['title'] = { [Op.like]: `%${where.title}%` }
      }
      if (where && where.tags) {
        whereQueryDetail['description'] = { [Op.like]: `%${where.tags}%` }
      }
      await HitsModel.findAndCountAll({
        where: whereQuery,
        limit,
        offset,
        include: [
          {
            model: TagsModel,
            as: 'tags',
            required:true,
            where: whereQueryDetail
          },
        ],
        raw:true
      }).then(result => {
        resolve(result)
      }).catch(e => {
        reject(e)
      })
    })
  }

  function removeItem (where) {
    return new Promise(async (resolve, reject) => {
      try {
        // Eliminado lógico
        let model = {
          status:'I'
        }
        await HitsModel.update(model, { where }).then(result => {
          resolve(true)
        }).catch(e => {
          reject(e)
        })
      }catch (e) {
        reject(e)
      }
    })
  }

  return {
    createListHits,
    findData,
    removeItem
  }
}