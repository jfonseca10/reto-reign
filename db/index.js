const { db } = require('./../config/index')
const setupDataBase = require('./libs/conexion')

//MODELS
const setupHitsModel = require('./models/hits')
const setupTagsModel = require('./models/tags')

//LIBS
const setupHits = require('./libs/hits')
const setupTags = require('./libs/tags')

//CONFIG DATABASE
module.exports = async function () {
  const config = {
    ...db.main,
    logging: console.log,
    query: {
      raw: true,
    },
  }
  const sequelize = setupDataBase(config)

  //CREATE MODELS
  const HitsModel = setupHitsModel(config)
  const TagsModel = setupTagsModel(config)

  //CREATE RELATIONS MODELS
  HitsModel.hasMany(TagsModel,{
    foreignKey: 'idHits',
    as: 'tags'
  })
  await sequelize.authenticate()

  //CREATE LIBS
  const Hits = setupHits(HitsModel, TagsModel)
  const Tags = setupTags(TagsModel)

  //RETURNS
  return {
    Hits,
    Tags
  }
}