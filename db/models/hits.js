const Sequelize = require('sequelize')
const setupDataBase = require('./../libs/conexion')

module.exports = function setupHitsModel(config){
  const sequelize = setupDataBase(config)
  return sequelize.define('hits',{
    idHits:{
      primaryKey: true,
      field: 'idHits',
      allowNull: false,
      type: Sequelize.STRING
    },
    story_id:{
      field: 'story_id',
      allowNull: true,
      type: Sequelize.INTEGER
    },
    author:{
      field: 'author',
      allowNull: true,
      type: Sequelize.STRING
    },
    title:{
      field: 'title',
      allowNull: true,
      type: Sequelize.STRING
    },
    container:{
      field: 'container',
      allowNull: true,
      type: Sequelize.TEXT,
    },
    currentDate:{
      field: 'currentDate',
      allowNull: true,
      type: Sequelize.DATE
    },
    status:{
      field: 'status',
      allowNull: true,
      type: Sequelize.STRING
    }
  },{
    timestamps: false,
    tableName: 'hits'
  })

}