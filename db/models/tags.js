const Sequelize = require('sequelize')
const setupDataBase = require('./../libs/conexion')

module.exports = function setupTagsModel(config) {
  const sequelize = setupDataBase(config)
  return sequelize.define('tags',{
    idTags:{
      primaryKey: true,
      field: 'idTags',
      allowNull: false,
      type: Sequelize.STRING
    },
    idHits: {
      type: Sequelize.STRING,
      allowNull: false,
      references: {
        model: 'hits',
        key: 'idHits'
      },
      field: 'idHits'
    },
    description:{
      field: 'description',
      allowNull: true,
      type: Sequelize.STRING
    },
  },{
    timestamps:false,
    tableName: 'tags'
  })
}