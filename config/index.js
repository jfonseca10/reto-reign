const minimist = require('minimist')
const dotenv = require('dotenv')
const params = minimist(process.argv)

const path = `${__dirname}/${params.prod && params.prod === 'true'
  ? '.env'
  : '.env.develop'}`

const index = dotenv.config({path}).parsed
module.exports = {
  db:{
    main:{
      dialect: index.DB_MAIN_DIALECT,
      database: index.DB_MAIN_DB,
      username: index.DB_MAIN_USER,
      password: index.DB_MAIN_PASSWD,
      host: index.DB_MAIN_HOST,
      port: index.DB_MAIN_PORT
    }
  },
  apiConfig:{
    port: index.API_PORT,
    secret: index.API_SECRET,
    bsr: index.API_BSR,
    sessionTime: index.API_SESSION_TIME,
    urlRest: index.API_URL_REST
  },
  validateSchema: {
    keyByField: true
  },
  security: {
    digest: '@ngul@r3sD3B@ck',
    encrypt: false
  },
}