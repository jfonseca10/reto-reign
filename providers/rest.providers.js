const axios = require('axios')
const {apiConfig} = require('./../config/index')

async function webServiceSearchData () {
  return new Promise((resolve, reject) => {
    axios(
      {
        url: apiConfig.urlRest,
        method: 'GET'
      }).then(function (data) {
      resolve(data.data.hits)
    }).catch(function (error) {
      reject(error)
    })
  })
}

module.exports = {
  webServiceSearchData,
}
