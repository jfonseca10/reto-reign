const http = require('http')
const https = require('https')
const fs = require('fs')
const cors = require('cors')
const express = require('express')
const cron = require('node-cron')
const asyncify = require('express-asyncify')
const minimist = require('minimist')
const moment = require('moment')
const params = minimist(process.argv)
const {apiConfig} = require('./config/index')
const logger = require('./utils/logger')
const db = require('./db/index')
const {webServiceSearchData} = require('./providers/rest.providers')

const hitsRoute = require('./src/routes/hits')

let envProd = false

if (params.prod && params.prod === 'true'){
  envProd = true
}

const app = asyncify(express())
let server, services, Hits, origins = ["*"]
if (envProd) {
  const privateKey = fs.readFileSync('/opt/openssl/nodejs.eeq.com.ec-key.pem',
    'utf8')
  const certificate = fs.readFileSync('/opt/openssl/nodejs.eeq.com.ec-crt.pem',
    'utf8')
  const ca = fs.readFileSync('/opt/openssl/nodejs.eeq.com.ec-crt.pem', 'utf8')
  const credentials = {
    key: privateKey,
    cert: certificate,
    ca,
  }
  server = https.createServer(credentials, app)
} else {
  server = http.createServer(app)
}

app.use(cors())
app.use(express.json({limit:'80mb'}))
app.use(express.urlencoded({limit: '60mb'}))
app.use('/search', hitsRoute)

let segundos = '0'
let minutos = '0'
let horas = '*/1'
let dias = '*'
let meses = '*'
let diaSemana = '*'

cron.schedule(`${segundos} ${minutos} ${horas} ${dias} ${meses} ${diaSemana}`, async () => {
  logger.info(`Api reto run ${moment().format('DD/MM/YYYY HH:mm:ss')}`)
  if (!services) {
    try {
      services = await db()
    } catch (e) {
      return (e)
    }
    Hits = services.Hits
  }
  const { createListHits } = Hits
  let result = await webServiceSearchData()
  logger.info(`Se consulto al servicio web ${result.length} registros. ${moment().format('DD/MM/YYYY HH:mm:ss')}`)
  let resultDb = await createListHits(result)
  logger.info(`Se subio ${resultDb} registros a postgres. ${moment().format('DD/MM/YYYY HH:mm:ss')}`)
})


function handledFatalError (err) {
  console.error(`[fatal error]: ${err.message}`)
  console.error(err.stack)
  process.exit(1)
}

if (!module.parent) {
  process.on('uncaughtException', handledFatalError)
  process.on('unhandledRejection', handledFatalError)
  server.listen(apiConfig.port, () => {
    console.log(`[api-server]: listening on port ${apiConfig.port}`)
    logger.info(`[api-server]: listening on port ${apiConfig.port}`)
  })
}

module.exports = server


