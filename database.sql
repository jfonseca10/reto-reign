-- Database: reto_db

-- DROP DATABASE IF EXISTS reto_db;

CREATE DATABASE reto_db
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
	
-- Table: public.hits

-- DROP TABLE IF EXISTS public.hits;

CREATE TABLE IF NOT EXISTS public.hits
(
    "idHits" character varying(150) COLLATE pg_catalog."default" NOT NULL,
    story_id bigint,
    author character varying(300) COLLATE pg_catalog."default",
    title character varying(300) COLLATE pg_catalog."default",
    "currentDate" timestamp with time zone,
    container text COLLATE pg_catalog."default",
    status character varying(1) COLLATE pg_catalog."default",
    CONSTRAINT hits_pkey PRIMARY KEY ("idHits")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.hits
    OWNER to postgres;
	
	-- Table: public.tags

-- DROP TABLE IF EXISTS public.tags;

CREATE TABLE IF NOT EXISTS public.tags
(
    "idTags" character varying(150) COLLATE pg_catalog."default" NOT NULL,
    description character varying(200) COLLATE pg_catalog."default" NOT NULL,
    "idHits" character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT tags_pkey PRIMARY KEY ("idTags"),
    CONSTRAINT fk_tags_hits FOREIGN KEY ("idHits")
        REFERENCES public.hits ("idHits") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tags
    OWNER to postgres;