## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Este proyecto es una api que consulta a travez de un servicio rest a otra api, la cual devuelve una lista de Hits los cuales tienen muchos Tags. Esta información se sube a una base de datos postgres y este proceso se repite cada hora con una tarea programada usando node-cron:

```javascript
// ┌────────────── second (optional)
// │ ┌──────────── minute
// │ │ ┌────────── hour
// │ │ │ ┌──────── day of month
// │ │ │ │ ┌────── month
// │ │ │ │ │ ┌──── day of week
// │ │ │ │ │ │
// │ │ │ │ │ │
// * * * * * *
```
Se creó dos endpoints:  
* POST: http://localhost:7075/search/getDataFilter este endpoint permite consultar la información guardada en la base de datos la cual deberá devolver la informacion filtrada por autor, titulo o tags y con la paginación indicada. Ejemplos:
```javascript
 {
  "page":"0",
    "size":"1",
    "where":{
    "author": "YesBox",
      "tags": "story",
      "title": "Show"
  }
}
```

```javascript
 {
  "page":"0",
    "size":"1",
    "where":{
    "author": "Yes",
      "tags": "story",
  }
}
```

```javascript
 {
  "page":"0",
    "size":"1"
 }
```

* PUT: http://localhost:7075/search/removeItem este endpoint permite remover la información guardada en la base de datos la cual no deberá aparecer de nuevo en una nueva consulta, primero se debe consultar en el endpoint anterior para capturar un id y remover un hit. Ejemplos:
```javascript
 {
  "idHits":"4ecda81d-2722-4c83-b12b-afc3cc323f05"
}
```

## Technologies
El proyecto esta creado con las siguientes tecnologias:
* Nodejs version: 16.13.2
* PostgreSQL 10.21, compiled by Visual C++ build 1800, 64-bit

## Setup
Primero se debe correr el script para crear la base de datos.
* Ejecutar comandos de archivo database.sql 

Posterior a esto el proyecto corre usando dependencias de npm:

```
$ git clone https://gitlab.com/jfonseca10/reto-reign.git
$ cd reto-reign/
$ npm install
$ npm start
```