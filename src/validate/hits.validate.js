const { Joi } = require('express-validation')
module.exports = {
  findByParameters: {
    body: Joi.object({
      where: Joi.object({
        author: Joi.string(),
        title: Joi.string(),
        tags: Joi.string()
      }),
      page: Joi.string(),
      size: Joi.string(),
    })
  },
  removeItem:{
    body: Joi.object({
      idHits: Joi.string().required()
    })
  }
}