const express = require('express')
const asyncify = require('express-asyncify')
const db = require('../../db')
const { apiValidate } = require('./../../libs/validate.config')
const validation = require('../validate/hits.validate')
const { sendRequest } = require('./../../libs/response.lib')

const api = asyncify(express.Router())
let service, Hits

api.use('*', async (req, res, next) => {
  if (!service){
    try {
      service = await db()
    }catch (e) {
      next(e)
    }
    Hits = service.Hits
  }
  next()
})

api.route('/getDataFilter').post(apiValidate(validation.findByParameters), async (req,res,next) => {
  const pageAsNumber = Number.parseInt(req.body.page)
  const sizeAsNumber = Number.parseInt(req.body.size)
  let page = 0, size = 5, where
  if (!Number.isNaN(pageAsNumber) && pageAsNumber > 0){
    page = pageAsNumber
  }
  if (!Number.isNaN(sizeAsNumber) && pageAsNumber > 0 && sizeAsNumber<5){
    size = pageAsNumber
  }
  where = req.body.where
  await Hits.findData({ where, page, size }).then((result) => {
    if (result) {
      return res.send(sendRequest({ data: result }))
    } else {
      return res.status(404).send(null)
    }
  }).catch((e) => {
    return next(e)
  })
})

api.route('/removeItem').put(apiValidate(validation.removeItem), async (req, res, next) => {
  const idHits = req.body
  const result = await Hits.removeItem(idHits)
  if (result) {
    return res.send(sendRequest({ data: 'Se removió con exito el registro.' }))
  } else {
    return res.status(404).send(null)
  }
})

module.exports = api